#include "sa.h"

#include <stdlib.h>

#include <algorithm>
#include <cmath>
#include <iostream>

#include "common.h"
#include "opt.h"

double probability(double f1, double f2, double temperature) {
  if (f2 < f1) {
    return 1.0;
  }

  return exp((f1 - f2) / temperature);
}

vertex_list simulated_annealing(TSPInstance tsp_instance, vertex_list path,
                                graph_matrix graph) {
  vertex_list curr_path = path;
  vertex_list best_path = curr_path;
  double best_len = evaluate_solution(best_path, tsp_instance);

  for (double t = 1000; t > 1; t *= 0.995) {
    vertex_list new_path = curr_path;

    int rnd_a = rand() % new_path.size();
    int rnd_b = rand() % new_path.size();

    std::iter_swap(new_path.begin() + rnd_a, new_path.begin() + rnd_b);

    double current_len = evaluate_solution(curr_path, tsp_instance);
    double neighbor_len = evaluate_solution(new_path, tsp_instance);

    double random = ((double)rand() / (RAND_MAX));
    if (probability(current_len, neighbor_len, t) > random) {
      curr_path = new_path;
      current_len = neighbor_len;
    }

    if (current_len < best_len) {
      best_path = curr_path;
      best_len = current_len;
    }
  }

  return best_path;
}

vertex_list tsp(TSPInstance tsp_instance) {
  // Create the adj matrix
  graph_matrix edges(tsp_instance.n_cities,
                     std::vector<double>(tsp_instance.n_cities, 0));

  // Looping through each city combination and computing its distance
  for (size_t ai = 0; ai < tsp_instance.n_cities; ai++) {
    auto city_a = tsp_instance.city_positions[ai];
    for (size_t bi = ai + 1; bi < tsp_instance.n_cities; bi++) {
      auto city_b = tsp_instance.city_positions[bi];

      double distance = 0;
      if (tsp_instance.dist_type == "EUC_2D") {
        distance = EUC_2D(city_a, city_b);
      } else if (tsp_instance.dist_type == "ATT") {
        distance = ATT(city_a, city_b);
      }

      // Add an edge both ways
      edges[ai][bi] = distance;
      edges[bi][ai] = distance;
    }
  }

  vertex_list best_path;
  double best_dist = 99999999999;

  for (int starting_point = 0; starting_point < (int)edges.size();
       starting_point++) {
    vertex_list path;
    std::vector<bool> visited(edges.size(), false);

    path.push_back(starting_point);
    visited[starting_point] = true;

    while (path.size() != edges.size()) {
      double min_dist = 9999999999;
      int min_dist_i = 0;

      for (int j = 0; j < (int)edges.size(); j++) {
        if (path[path.size() - 1] == j || visited[j]) continue;
        if (edges[path[path.size() - 1]][j] < min_dist) {
          min_dist = edges[path[path.size() - 1]][j];
          min_dist_i = j;
        }
      }

      path.push_back(min_dist_i);
      visited[min_dist_i] = true;
    }

    double curr_dist = evaluate_solution(path, tsp_instance);
    if (curr_dist < best_dist) {
      best_dist = curr_dist;
      best_path = path;
    }
  }

  return simulated_annealing(tsp_instance, best_path, edges);
}