#ifndef OPT3_H
#define OPT3_H

#include "common.h"
#include "graph.h"

double probability(double f1, double f2, double temp);
vertex_list simulated_annealing(TSPInstance tsp_instance, vertex_list path,
                                graph_matrix graph);

// Runs the heuristic to solve the TSP problem on the given
// graph
vertex_list tsp(TSPInstance tsp_instance);

#endif